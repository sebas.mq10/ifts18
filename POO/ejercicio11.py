from abc import ABC, abstractmethod
from datetime import datetime

class CuentaBancaria(ABC):
    def __init__(self, nro_cuenta, alias, cbu, saldo):
        self._nro_cuenta = nro_cuenta
        self._alias = alias
        self._cbu = cbu
        self._saldo = saldo
        self._movimientos = []

    @property
    def nro_cuenta(self):
        return self._nro_cuenta

    @property
    def saldo(self):
        return self._saldo

    def consultar_saldo(self):
        return self.saldo

    def registrar_movimiento(self, tipo, monto):
        fecha = datetime.now()
        movimiento = (fecha, tipo, monto, self.saldo)
        self._movimientos.append(movimiento)

    @abstractmethod
    def extraer(self, monto):
        pass

    @abstractmethod
    def transferir(self, monto, cuenta_destino):
        pass

class CajaDeAhorro(CuentaBancaria):
    def __init__(self, nro_cuenta, alias, cbu, saldo, monto_limite_extracciones, monto_limite_transferencias, cant_extracciones_disponibles, cant_transferencias_disponibles):
        super().__init__(nro_cuenta, alias, cbu, saldo)
        self._monto_limite_extracciones = monto_limite_extracciones
        self._monto_limite_transferencias = monto_limite_transferencias
        self._cant_extracciones_disponibles = cant_extracciones_disponibles
        self._cant_transferencias_disponibles = cant_transferencias_disponibles

    @property
    def cant_extracciones_disponibles(self):
        return self._cant_extracciones_disponibles

    @property
    def cant_transferencias_disponibles(self):
        return self._cant_transferencias_disponibles

    def extraer(self, monto):
        if monto > 0 and monto <= self.saldo and monto <= self._monto_limite_extracciones and self._cant_extracciones_disponibles > 0:
            self._saldo -= monto
            self._cant_extracciones_disponibles -= 1
            self.registrar_movimiento("extracción", monto)
            return True
        return False

    def transferir(self, monto, cuenta_destino):
        if monto > 0 and monto <= self.saldo and monto <= self._monto_limite_transferencias and self._cant_transferencias_disponibles > 0:
            self._saldo -= monto
            self._cant_transferencias_disponibles -= 1
            cuenta_destino._saldo += monto
            self.registrar_movimiento("transferencia", monto)
            return True
        return False

class CuentaCorriente(CuentaBancaria):
    def __init__(self, nro_cuenta, alias, cbu, saldo, monto_maximo_descubierto):
        super().__init__(nro_cuenta, alias, cbu, saldo)
        self._monto_maximo_descubierto = monto_maximo_descubierto

    def extraer(self, monto):
        if monto > 0 and monto <= self.saldo + self._monto_maximo_descubierto:
            self._saldo -= monto
            self.registrar_movimiento("extracción", monto)
            return True
        return False

    def transferir(self, monto, cuenta_destino):
        if monto > 0 and monto <= self.saldo + self._monto_maximo_descubierto:
            self._saldo -= monto
            cuenta_destino._saldo += monto
            self.registrar_movimiento("transferencia", monto)
            return True
        return False

class Cliente:
    def __init__(self, razon_social, cuit, tipo_persona, domicilio):
        self._razon_social = razon_social
        self._cuit = cuit
        self._tipo_persona = tipo_persona
        self._domicilio = domicilio
        self._cuentas_bancarias = []

    @property
    def cuentas_bancarias(self):
        return self._cuentas_bancarias

    def crear_nueva_cuenta_bancaria(self, cuenta):
        self._cuentas_bancarias.append(cuenta)

class Banco:
    def __init__(self, nombre, domicilio):
        self._nombre = nombre
        self._domicilio = domicilio
        self._clientes = []

    @property
    def clientes(self):
        return self._clientes

    def crear_nuevo_cliente(self, cliente):
        self._clientes.append(cliente)

def main():
    banco = Banco("Mi Banco", "Dirección del Banco")

    cliente1 = Cliente("Cliente 1", "12345678901", "Física", "Domicilio 1")
    cliente2 = Cliente("Cliente 2", "98765432109", "Jurídica", "Domicilio 2")

    caja_ahorro1 = CajaDeAhorro("001", "Caja Ahorro 1", "CBU001", 1000, 2000, 3000, 5, 10)
    cuenta_corriente1 = CuentaCorriente("002", "Cuenta Corriente 1", "CBU002", 2000, 1000)

    cliente1.crear_nueva_cuenta_bancaria(caja_ahorro1)
    cliente1.crear_nueva_cuenta_bancaria(cuenta_corriente1)

    caja_ahorro2 = CajaDeAhorro("003", "Caja Ahorro 2", "CBU003", 1500, 2500, 3500, 8, 12)
    cuenta_corriente2 = CuentaCorriente("004", "Cuenta Corriente 2", "CBU004", 2500, 1500)

    cliente2.crear_nueva_cuenta_bancaria(caja_ahorro2)
    cliente2.crear_nueva_cuenta_bancaria(cuenta_corriente2)

    banco.crear_nuevo_cliente(cliente1)
    banco.crear_nuevo_cliente(cliente2)

    for cliente in banco.clientes:
        print(f"Cliente: {cliente._razon_social}, CUIT: {cliente._cuit}, Tipo: {cliente._tipo_persona}, Domicilio: {cliente._domicilio}")
        for cuenta in cliente.cuentas_bancarias:
            print(f"- Cuenta: {cuenta.nro_cuenta}, Alias: {cuenta._alias}, Saldo: {cuenta.saldo}")
            if isinstance(cuenta, CajaDeAhorro):
                print(f"  - Límite Extracciones: {cuenta._monto_limite_extracciones}, Límite Transferencias: {cuenta._monto_limite_transferencias}")
                print(f"  - Extracciones Disponibles: {cuenta._cant_extracciones_disponibles}, Transferencias Disponibles: {cuenta._cant_transferencias_disponibles}")
            elif isinstance(cuenta, CuentaCorriente):
                print(f"  - Límite Descubierto: {cuenta._monto_maximo_descubierto}")
            print("  - Movimientos:")
            for movimiento in cuenta._movimientos:
                fecha, tipo, monto, saldo = movimiento
                print(f"    - Fecha: {fecha}, Tipo: {tipo}, Monto: {monto}, Saldo: {saldo}")

if __name__ == "__main":
    main()
