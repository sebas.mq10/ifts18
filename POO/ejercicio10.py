import random
import string

class Password:
    # Atributos de clase privados
    _LONGITUD = 8
    _CARACTERES_VALIDOS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"

    def __init__(self, longitud=0):
        self._longitud = (
            self._LONGITUD if longitud < 6 or longitud > 15 else longitud
        )
        self._contraseña = self.generarPassword()

    @property
    def longitud(self):
        return self._longitud

    @longitud.setter
    def longitud(self, value):
        if 6 <= value <= 15:
            self._longitud = value

    @property
    def contraseña(self):
        return self._contraseña

    def esFuerte(self):
        mayusculas = 0
        minusculas = 0
        numeros = 0
        caracteres_especiales = 0

        for char in self._contraseña:
            if char.isupper():
                mayusculas += 1
            elif char.islower():
                minusculas += 1
            elif char.isdigit():
                numeros += 1
            elif char in "!@#$%^&*()+-<=>?":
                caracteres_especiales += 1

        return (
            mayusculas > 1
            and minusculas > 1
            and numeros > 1
            and caracteres_especiales > 1
        )

    def generarPassword(self):
        return "".join(random.choice(self._CARACTERES_VALIDOS) for _ in range(self._longitud))

    def __str__(self):
        return f"{self._contraseña} - {self.esFuerte()}"

def main():
    contraseñas = []

    cantidad = int(input("Ingrese la cantidad de contraseñas a generar: "))
    for _ in range(cantidad):
        longitud = int(input("Ingrese la longitud de la contraseña (6-15): "))
        contraseña = Password(longitud)
        contraseñas.append(contraseña)

    for contraseña in contraseñas:
        print(contraseña)

if __name__ == "__main__":
    main()
