
import math

class FiguraGeometrica:
    def __init__(self, color_fondo, color_borde):
        self.color_fondo = color_fondo
        self.color_borde = color_borde

    def area(self):
        pass  # Cada figura geométrica debe implementar su propio cálculo de área

    def perimetro(self):
        pass  # Cada figura geométrica debe implementar su propio cálculo de perímetro

class Rectangulo(FiguraGeometrica):
    def __init__(self, color_fondo, color_borde, base, altura):
        super().__init__(color_fondo, color_borde)
        self.base = base
        self.altura = altura

    def area(self):
        return self.base * self.altura

    def perimetro(self):
        return 2 * (self.base + self.altura)

class Circulo(FiguraGeometrica):
    def __init__(self, color_fondo, color_borde, radio):
        super().__init__(color_fondo, color_borde)
        self.radio = radio

    def area(self):
        return math.pi * self.radio ** 2

    def perimetro(self):
        return 2 * math.pi * self.radio

class Triangulo(FiguraGeometrica):
    def __init__(self, color_fondo, color_borde, base, altura):
        super().__init__(color_fondo, color_borde)
        self.base = base
        self.altura = altura

    def area(self):
        return 0.5 * self.base * self.altura

    def perimetro(self):
        # No se especifica el tipo de triángulo, por lo que no se puede calcular el perímetro con solo base y altura
        return None  

# Ejemplo de uso:
rectangulo = Rectangulo("rojo", "negro", 5, 10)
print("Rectángulo - Área:", rectangulo.area())
print("Rectángulo - Perímetro:", rectangulo.perimetro())

circulo = Circulo("azul", "blanco", 7)
print("Círculo - Área:", circulo.area())
print("Círculo - Perímetro:", circulo.perimetro())

triangulo = Triangulo("verde", "gris", 6, 8)
print("Triángulo - Área:", triangulo.area())
print("Triángulo - Perímetro:", triangulo.perimetro())