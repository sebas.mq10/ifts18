# Escribe un programa en Python que solicite al usuario que ingrese 5 números enteros.
# Luego imprimir el máximo y el mínimo de los valores ingresados. El programa deberá permitir el ingreso de valores iguales.

numeros = []

for i in range(5):
    numero = int(input(f"Ingrese el número {i + 1}: "))
    numeros.append(numero)

    maximo = max(numeros)
    minimo = min(numeros)

print("El máximo valor ingresado es:", maximo)
print("El mínimo valor ingresado es:", minimo)