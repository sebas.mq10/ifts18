# Escribe un programa en Python que solicite 5 números enteros al usuario. El mismo debe indicar si
# entre dichos valores hay números duplicados o no, imprimiendo por pantalla “HAY DUPLICADOS” o “SON TODOS DISTINTOS”.

num1 = int(input('Ingrese el primer numero: '))
num2 = int(input('Ingrese el segundo numero: '))
num3 = int(input('Ingrese el tercer numero: '))
num4 = int(input('Ingrese el cuarto numero: '))
num5 = int(input('Ingrese el quinto numero: '))

lista_numeros = [num1,num2,num3,num4,num5]

set_numeros = set(lista_numeros)

if len(lista_numeros) == len(set_numeros):
    print("La lista tiene no valores duplicados")
else:
    print("La lista tiene valores duplicados")

print(lista_numeros)