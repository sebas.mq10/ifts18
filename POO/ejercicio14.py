import sqlite3

def conectar():
    conexion = sqlite3.connect("empleados.db")
    return conexion

def crearTabla(conexion):
    cursor = conexion.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS empleados (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            nro_legajo INTEGER NOT NULL UNIQUE,
            dni INTEGER NOT NULL UNIQUE,
            nombre TEXT NOT NULL,
            apellido TEXT NOT NULL,
            area TEXT NOT NULL
        )
    ''')
    conexion.commit()

def insertarEmpleado(conexion):
    nro_legajo = int(input("Ingrese el número de legajo: "))
    dni = int(input("Ingrese el DNI: "))
    nombre = input("Ingrese el nombre: ")
    apellido = input("Ingrese el apellido: ")
    area = input("Ingrese el área: ")

    cursor = conexion.cursor()
    cursor.execute('''
        INSERT INTO empleados (nro_legajo, dni, nombre, apellido, area)
        VALUES (?, ?, ?, ?, ?)
    ''', (nro_legajo, dni, nombre, apellido, area))

    conexion.commit()
    print("Registro de empleado insertado correctamente.")

def seleccionarPorDNI(conexion):
    dni = int(input("Ingrese el número de DNI: "))
    cursor = conexion.cursor()
    cursor.execute('SELECT * FROM empleados WHERE dni = ?', (dni,))
    empleado = cursor.fetchone()

    if empleado:
        print("Registro encontrado:")
        print(empleado)
    else:
        print("No se encontró ningún empleado con ese DNI.")

def seleccionarTodos(conexion):
    cursor = conexion.cursor()
    cursor.execute('SELECT * FROM empleados')
    empleados = cursor.fetchall()

    if empleados:
        print("Registros encontrados:")
        for empleado in empleados:
            print(empleado)
    else:
        print("No hay empleados registrados.")

def modificarArea(conexion):
    nro_legajo = int(input("Ingrese el número de legajo del empleado a modificar: "))
    nueva_area = input("Ingrese la nueva área: ")

    cursor = conexion.cursor()
    cursor.execute('''
        UPDATE empleados
        SET area = ?
        WHERE nro_legajo = ?
    ''', (nueva_area, nro_legajo))

    conexion.commit()
    print("Área del empleado actualizada correctamente.")

def eliminarEmpleado(conexion):
    nro_legajo = int(input("Ingrese el número de legajo del empleado a eliminar: "))

    cursor = conexion.cursor()
    cursor.execute('DELETE FROM empleados WHERE nro_legajo = ?', (nro_legajo,))

    conexion.commit()
    print("Empleado eliminado correctamente.")

def cerrarConexion(conexion):
    conexion.close()
    print("Conexión cerrada.")

def main():
    conexion = conectar()
    crearTabla(conexion)

    while True:
        print("\nMenu:")
        print("1. Insertar un registro de empleado")
        print("2. Seleccionar un registro de empleado por DNI")
        print("3. Seleccionar todos los empleados")
        print("4. Modificar el área de un empleado por número de legajo")
        print("5. Eliminar un empleado por número de legajo")
        print("6. Finalizar")

        opcion = input("Ingrese la opción: ")

        if opcion == "1":
            insertarEmpleado(conexion)
        elif opcion == "2":
            seleccionarPorDNI(conexion)
        elif opcion == "3":
            seleccionarTodos(conexion)
        elif opcion == "4":
            modificarArea(conexion)
        elif opcion == "5":
            eliminarEmpleado(conexion)
        elif opcion == "6":
            cerrarConexion(conexion)
            break
        else:
            print("Opción no válida. Intente de nuevo.")

if __name__ == "__main__":
    main()