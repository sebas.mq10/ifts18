import random

class Password:
    # Atributos de clase privados
    __LONGITUD = 8
    __CARACTERES_VALIDOS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"

    def __init__(self, longitud=0):
        if longitud < 6 or longitud > 15:
            self.__longitud = self.__LONGITUD
        else:
            self.__longitud = longitud
        self.__contraseña = self.generarPassword()

    def esFuerte(self):
        mayusculas = 0
        minusculas = 0
        numeros = 0
        caracteres_especiales = 0

        for char in self.__contraseña:
            if char.isupper():
                mayusculas += 1
            elif char.islower():
                minusculas += 1
            elif char.isdigit():
                numeros += 1
            elif char in "!@#$%^&*()+-<=>?":
                caracteres_especiales += 1

        return (
            mayusculas > 1
            and minusculas > 1
            and numeros > 1
            and caracteres_especiales > 1
        )

    def generarPassword(self):
        return "".join(random.choice(self.__CARACTERES_VALIDOS) for _ in range(self.__longitud))

    def __str__(self):
        return f"{self.__contraseña} - {self.esFuerte()}"

# Crear una lista de objetos de tipo Password
contraseñas = []

# Crear instancias de Password y agregarlas a la lista
cantidad = int(input("Ingrese la cantidad de contraseñas a generar: "))
for _ in range(cantidad):
    longitud = int(input("Ingrese la longitud de la contraseña (6-15): "))
    contraseña = Password(longitud)
    contraseñas.append(contraseña)

# Mostrar cada una de las contraseñas creadas y si es o no fuerte
for contraseña in contraseñas:
    print(contraseña)
