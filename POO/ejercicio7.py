""" Crear una clase llamada Persona con los siguientes atributos privados>

-nombre
-edad
- dni

Y los siguientes metodos:

- mostrar_edad(): retorna la edad de la persona
- es_mayor_edad(): retorna True si edad es mayor o igual a 18,o False si es menor a 18.

El metodo constructor __init__ de la clase debe recibir y asigna los valores a cada uno
de los atributos privados de la clase. """


class Persona:
    def __init__(self,nombre,edad,dni):
        self._nombre = nombre
        self._edad = edad
        self._dni = dni

    def mostrar_edad(self):
        return self._edad
    
    def es_mayor_edad(self):
        return self._edad >= 18
    

persona1 = Persona("Seba",34,3494213)

print(persona1._nombre)
print(persona1.mostrar_edad())
print(persona1.es_mayor_edad())