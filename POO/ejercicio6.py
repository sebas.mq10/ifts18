# Ejercicio 6
# Usando la función del ejercicio anterior, escribir un programa que pida al usuario dos intervalos expresados 
# en horas, minutos y segundos, sume sus duraciones, y muestre por pantalla la duración total en horas, minutos y segundos.


from ejercicio5 import calculaSegundos

def sumarIntervalos():
    horasPrimerIntervalo = int(input('Ingrese horas del primer intervalo: '))
    minutosPrimerIntervalo = int(input('Ingrese minutos del primer intervalo: '))
    segundosPrimerIntervalo = int(input('Ingrese segundos del primer intervalo: '))

    horasSegundoIntervalo = int(input('Ingrese horas del segundo intervalo: '))
    minutosSegundoIntervalo = int(input('Ingrese minutos del segundo intervalo: '))
    segundosSegundoIntervalo = int(input('Ingrese segundos del segundo intervalo: '))

    duracionTotalEnSegundos = calculaSegundos(horasPrimerIntervalo, minutosPrimerIntervalo, segundosPrimerIntervalo) + calculaSegundos(horasSegundoIntervalo, minutosSegundoIntervalo, segundosSegundoIntervalo)

    horasTotal = duracionTotalEnSegundos // 3600
    minutosTotal = (duracionTotalEnSegundos % 3600) // 60
    segundosTotal = duracionTotalEnSegundos % 60

    print(f"La duración total es: {horasTotal} horas, {minutosTotal} minutos, {segundosTotal} segundos.")


sumarIntervalos()