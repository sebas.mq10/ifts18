# Escribir un programa en Python que pida al usuario que ingrese las medidas de la base y la altura de un rectángulo y muestre:
# 1.El perímetro del rectángulo
# 2.El área del rectángulo


def calculaAreaYPerimetro():
    base = float(input('Ingrese cuanto mide la base del rectángulo: '))
    altura = float(input('Ingrese cuanto mide la altura del rectángulo: '))

    perimetro = 2 * (base + altura)
    area = base * altura

    print('El perímetro es: ', perimetro, 'cm')
    print('El área es: ', area, 'cm')


calculaAreaYPerimetro()